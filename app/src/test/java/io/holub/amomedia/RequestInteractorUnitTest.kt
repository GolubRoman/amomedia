package io.holub.amomedia

import android.os.Build
import io.holub.amomedia.common.di.AppComponent
import io.holub.amomedia.common.di.AppModule
import io.holub.amomedia.common.di.DaggerAppComponent
import io.holub.amomedia.common.io.db.Prefs
import io.reactivex.schedulers.Schedulers
import junit.framework.Assert.assertTrue
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.robolectric.RobolectricTestRunner
import org.robolectric.RuntimeEnvironment
import org.robolectric.annotation.Config
import java.util.*

@RunWith(RobolectricTestRunner::class)
@Config(manifest = Config.NONE, sdk = [Build.VERSION_CODES.O_MR1])
class RequestInteractorUnitTest {

    private var appComponent: AppComponent? = null

    @Before
    fun init() {
        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(RuntimeEnvironment.application))
            .build()
    }

    @Test
    fun searchByQueryTest() {
        val requestInteractor = appComponent!!.requestInteractor()
        val requestDao = appComponent!!.requestDao()
        val query = UUID.randomUUID().toString()

        requestInteractor.searchByQuery(query)
            .blockingGet()

        val queryResults = requestDao.observeQueryResultsMaybe()
            .subscribeOn(Schedulers.io())
            .blockingGet()

        assertTrue(queryResults != null
                && queryResults.isNotEmpty()
                && queryResults.none { !it.title.contains(query) })
    }

    @Test
    fun observeLastRequestTest() {
        val requestInteractor = appComponent!!.requestInteractor()
        val prefs = appComponent!!.prefs()
        val request = createRandomRequest()

        prefs.put(Prefs.Key.INDEX, request.index).blockingGet()
        prefs.put(Prefs.Key.QUERY, request.query).blockingGet()

        val newRequest = requestInteractor.observeLastRequest()
            .blockingGet()

        assertTrue(newRequest != null
                && newRequest.index == request.index
                && newRequest.query == request.query)
    }

}