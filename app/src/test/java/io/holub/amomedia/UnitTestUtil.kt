package io.holub.amomedia

import com.annimon.stream.Collectors
import com.annimon.stream.Stream
import io.holub.amomedia.common.io.dto.response.Item
import io.holub.amomedia.common.io.dto.response.ResponseDto
import io.holub.amomedia.common.io.model.QueryResult
import io.holub.amomedia.common.io.model.Request
import io.reactivex.Observable
import io.reactivex.schedulers.Schedulers
import java.util.*

fun createRandomRequest(): Request {
    return Request(
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        Random().nextInt()
    )
}

fun createRandomQueryResult(): QueryResult {
    return QueryResult(
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        UUID.randomUUID().toString(),
        Random().nextLong()
    )
}

fun createRandomQueryResult(count: Int): List<QueryResult> {
    var list = mutableListOf<QueryResult>()
    Observable.range(0, count)
        .flatMap {
            Observable.fromCallable {
                list.add(createRandomQueryResult())
            }
        }.subscribeOn(Schedulers.io())
        .blockingLast()
    return list
}

fun createRandomResponseDto(query: String) = ResponseDto(null,
    Stream.range(0, 10)
        .map {
            Item(UUID.randomUUID().toString(), null, null, null, null, null, null,
                "https://www.google.com/search?q=$query", null,
                Stream.range(0, 10)
                    .map { query }
                    .collect(Collectors.joining(",")), "$query " + UUID.randomUUID().toString())
        }
        .toList(), null, null, null, null)

