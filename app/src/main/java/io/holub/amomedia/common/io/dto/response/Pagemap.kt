package io.holub.amomedia.common.io.dto.response

data class Pagemap(
    val cse_image: List<CseImage>?,
    val cse_thumbnail: List<CseThumbnail>?,
    val metatags: List<Metatag>?
)