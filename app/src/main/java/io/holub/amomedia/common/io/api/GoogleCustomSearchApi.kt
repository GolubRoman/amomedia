package io.holub.amomedia.common.io.api

import io.holub.amomedia.common.io.dto.response.ResponseDto
import io.reactivex.Single
import retrofit2.http.*

interface GoogleCustomSearchApi {

    @GET(".")
    fun getResponseList(@Query("key") apiKey: String,
                        @Query("cx") customSearchEngineId: String,
                        @Query("q") query: String,
                        @Query("start") index: Int): Single<ResponseDto>
}
