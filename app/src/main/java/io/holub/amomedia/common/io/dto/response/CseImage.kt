package io.holub.amomedia.common.io.dto.response

data class CseImage(
    val src: String?
)