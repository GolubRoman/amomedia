package io.holub.amomedia.common.io.api

import com.annimon.stream.Collectors
import com.annimon.stream.Stream
import io.holub.amomedia.common.io.dto.response.Item
import io.holub.amomedia.common.io.dto.response.ResponseDto
import io.reactivex.Single
import java.util.*
import java.util.concurrent.TimeUnit

class TestGoogleCustomSearchApi: GoogleCustomSearchApi {

    override fun getResponseList(
        apiKey: String,
        customSearchEngineId: String,
        query: String,
        index: Int
    ): Single<ResponseDto> = Single.just(
        ResponseDto(null,
            Stream.range(0, 10)
                .map { Item(UUID.randomUUID().toString(), null, null, null, null, null, null,
                    "https://www.google.com/search?q=$query", null,
                    Stream.range(0, 10)
                        .map { query }
                        .collect(Collectors.joining( "," )), "$query " + UUID.randomUUID().toString()) }
                .toList()
            , null, null, null, null)
    ).delay(4, TimeUnit.SECONDS)

}