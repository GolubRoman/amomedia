package io.holub.amomedia.common.util

import android.animation.Animator
import android.animation.ObjectAnimator
import android.support.v7.widget.RecyclerView
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import io.holub.amomedia.R
import io.holub.amomedia.common.view.AnimatorEndListener
import io.holub.amomedia.common.view.AnimatorStartListener
import io.holub.amomedia.common.view.GlideApp

fun initImageViewViaGlide(imageView: ImageView, photoUrl: String) {
    GlideApp.with(imageView.context)
        .load(photoUrl)
        .placeholder(R.drawable.ic_page_not_found)
        .into(imageView)
}

fun showEmptyListView(recyclerView: RecyclerView?, emptyListView: ViewGroup?) {
    recyclerView?.visibility = View.INVISIBLE
    emptyListView?.visibility = View.VISIBLE
}

fun hideEmptyListView(recyclerView: RecyclerView?, emptyListView: ViewGroup?) {
    emptyListView?.visibility = View.INVISIBLE
    recyclerView?.visibility = View.VISIBLE
}

fun showOnSearchProgressView(viewList: List<View?>, currentAnimator: ObjectAnimator?): ObjectAnimator {
    currentAnimator?.cancel()
    return viewList.map {
        val alpha = ObjectAnimator.ofFloat(it, "alpha", 0f, 1f)
        alpha.duration = 1000
        alpha.addListener(object : AnimatorStartListener {
                override fun onAnimationStart(animation: Animator?) {
                    it?.visibility = View.VISIBLE
                }
            })
        alpha.start()
        alpha
    }.last()
}

fun hideOnSearchProgressView(viewList: List<View?>, currentAnimator: ObjectAnimator?): ObjectAnimator {
    currentAnimator?.cancel()
    return viewList.map {
        val alpha = ObjectAnimator.ofFloat(it, "alpha", 1f, 0f)
        alpha.duration = 1000
        alpha.addListener(object : AnimatorEndListener {
            override fun onAnimationEnd(animation: Animator?) {
                it?.visibility = View.INVISIBLE
            }
        })
        alpha.start()
        alpha
    }.last()
}

