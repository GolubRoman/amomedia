package io.holub.amomedia.common.util

import android.content.Context
import android.net.ConnectivityManager
import io.holub.amomedia.common.App

fun isInternetConnectionUnavailable() = (App.instance
    .appComponent()
    .context()
    .getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager)
    .activeNetworkInfo == null
