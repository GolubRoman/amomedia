package io.holub.amomedia.common.io.api

import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor

fun buildClient(): OkHttpClient {
    return OkHttpClient.Builder()
        .addInterceptor(buildInterceptor())
        .build()
}

private fun buildInterceptor(): Interceptor {
    val interceptor = HttpLoggingInterceptor()
    interceptor.level = HttpLoggingInterceptor.Level.BODY
    return interceptor
}