package io.holub.amomedia.common.io.dto.response

import com.google.gson.annotations.SerializedName

data class Metatag(
    @SerializedName("og:description")
    val ogDescription: String?,
    @SerializedName("og:imagee")
    val ogImage: String?,
    @SerializedName("og:image:height")
    val ogImageHeight: String?,
    @SerializedName("og:image:width")
    val ogImageWidth: String?,
    @SerializedName("og:title")
    val ogTitle: String?,
    @SerializedName("og:type")
    val ogType: String?,
    @SerializedName("og:url")
    val ogUrl: String?,
    @SerializedName("twitter:card")
    val twitterCard: String?,
    @SerializedName("twitter:description")
    val twitterDescription: String?,
    @SerializedName("twitter:image")
    val twitterImage: String?,
    @SerializedName("twitter:site")
    val twitterSite: String?,
    @SerializedName("twitter:title")
    val twitterTitle: String?,
    val viewport: String?
)