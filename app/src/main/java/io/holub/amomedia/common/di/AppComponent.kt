package io.holub.amomedia.common.di

import android.content.Context
import dagger.Component
import io.holub.amomedia.common.error.ErrorHandler
import io.holub.amomedia.common.interactor.RequestInteractor
import io.holub.amomedia.common.io.api.GoogleCustomSearchApi
import io.holub.amomedia.common.io.db.Prefs
import io.holub.amomedia.common.io.db.RequestDao
import io.holub.amomedia.common.io.db.RoomDatabase
import javax.inject.Singleton

@Singleton
@Component(modules = [(AppModule::class)])
interface AppComponent {

    fun requestInteractor(): RequestInteractor

    fun googleCustomSearchApi(): GoogleCustomSearchApi

    fun prefs(): Prefs

    fun requestDao(): RequestDao

    fun roomDatabase(): RoomDatabase

    fun errorHandler(): ErrorHandler

    fun context(): Context

}
