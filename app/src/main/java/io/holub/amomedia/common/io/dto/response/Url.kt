package io.holub.amomedia.common.io.dto.response

data class Url(
    val template: String?,
    val type: String?
)