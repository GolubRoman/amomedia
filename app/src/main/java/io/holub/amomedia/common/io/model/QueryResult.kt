package io.holub.amomedia.common.io.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import io.holub.amomedia.common.io.dto.response.Pagemap

@Entity
data class QueryResult (
    @NonNull
    @PrimaryKey
    var id: String,
    var title: String,
    var link: String,
    var snippet: String,
    var image: String,
    var fetchDate: Long)