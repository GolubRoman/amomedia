package io.holub.amomedia.common.di

import android.arch.persistence.room.Room
import android.content.Context
import dagger.Module
import dagger.Provides
import io.holub.amomedia.BuildConfig
import io.holub.amomedia.common.App
import io.holub.amomedia.common.Flavors
import io.holub.amomedia.common.error.ErrorHandler
import io.holub.amomedia.common.interactor.RequestInteractor
import io.holub.amomedia.common.io.api.TestGoogleCustomSearchApi
import io.holub.amomedia.common.io.api.GoogleCustomSearchApi
import io.holub.amomedia.common.io.api.buildClient
import io.holub.amomedia.common.io.db.Prefs
import io.holub.amomedia.common.io.db.RequestDao
import io.holub.amomedia.common.io.db.RoomDatabase
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class AppModule(private val context: Context) {

    @Provides
    @Singleton
    fun provideContext(): Context {
        return context
    }

    @Provides
    @Singleton
    fun provideRequestInteractor(
        context: Context,
        requestDao: RequestDao,
        googleCustomSearchApi: GoogleCustomSearchApi,
        prefs: Prefs
    ): RequestInteractor {
        return RequestInteractor(context, requestDao, googleCustomSearchApi, prefs)
    }

    @Provides
    @Singleton
    fun provideRequestDao(roomDatabase: RoomDatabase): RequestDao {
        return roomDatabase.getRequestDao()
    }

    @Provides
    @Singleton
    fun provideErrorHandler(context: Context): ErrorHandler {
        return ErrorHandler(context)
    }

    @Provides
    @Singleton
    fun provideRoomDatabase(context: Context): RoomDatabase {
        return Room.databaseBuilder(context, RoomDatabase::class.java, RoomDatabase::class.java.simpleName)
            .fallbackToDestructiveMigration()
            .build()
    }

    @Provides
    @Singleton
    fun provideGoogleCustomSearchApi(): GoogleCustomSearchApi {
        return if (BuildConfig.FLAVOR == Flavors.PROD.flavorName) {
            Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .baseUrl(App.API_BASE_URL)
                .client(buildClient())
                .build()
                .create(GoogleCustomSearchApi::class.java)
        } else {
            TestGoogleCustomSearchApi()
        }
    }

    @Provides
    @Singleton
    fun providePrefs(context: Context): Prefs {
        return Prefs(context)
    }
}
