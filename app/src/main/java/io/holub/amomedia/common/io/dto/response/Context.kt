package io.holub.amomedia.common.io.dto.response

data class Context(
    val title: String?
)