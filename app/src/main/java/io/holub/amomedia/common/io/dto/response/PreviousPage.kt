package io.holub.amomedia.common.io.dto.response

data class PreviousPage(
    val count: Int?,
    val cx: String?,
    val inputEncoding: String?,
    val outputEncoding: String?,
    val safe: String?,
    val searchTerms: String?,
    val startIndex: Int?,
    val title: String?,
    val totalResults: String?
)