package io.holub.amomedia.common.io.dto.response

data class Queries(
    val nextPage: List<NextPage>?,
    val previousPage: List<PreviousPage>?,
    val request: List<Request>?
)