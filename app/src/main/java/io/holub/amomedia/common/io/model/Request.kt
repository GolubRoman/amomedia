package io.holub.amomedia.common.io.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity
data class Request (
    @NonNull
    @PrimaryKey
    var id: String,
    var query: String,
    var index: Int)