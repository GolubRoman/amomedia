package io.holub.amomedia.common.io.dto.response

data class CseThumbnail(
    val height: String?,
    val src: String?,
    val width: String?
)