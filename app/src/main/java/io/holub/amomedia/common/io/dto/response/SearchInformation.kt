package io.holub.amomedia.common.io.dto.response

data class SearchInformation(
    val formattedSearchTime: String?,
    val formattedTotalResults: String?,
    val searchTime: Double?,
    val totalResults: String?
)