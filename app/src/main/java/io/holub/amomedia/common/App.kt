package io.holub.amomedia.common

import android.annotation.SuppressLint
import android.app.Application
import com.crashlytics.android.Crashlytics
import com.tspoon.traceur.Traceur
import io.fabric.sdk.android.Fabric
import io.holub.amomedia.common.di.AppComponent
import io.holub.amomedia.common.di.AppModule
import io.holub.amomedia.common.di.DaggerAppComponent

class App : Application() {

    private lateinit var appComponent: AppComponent

    @SuppressLint("CheckResult")
    override fun onCreate() {
        super.onCreate()
        Traceur.enableLogging()
        Fabric.with(this, Crashlytics())
        instance = this

        appComponent = DaggerAppComponent.builder()
            .appModule(AppModule(applicationContext))
            .build()
    }

    fun appComponent(): AppComponent {
        return appComponent
    }

    companion object {

        lateinit var instance: App
            private set

        const val GOOGLE_SEARCH_KEY = "AIzaSyDbHuNsKnNm2ihzWGVlsMXMGjn7S8z6Atg"
        const val API_BASE_URL = "https://www.googleapis.com/customsearch/v1/"
        const val CUSTOM_SEARCH_ENGINE_ID = "009491443834515789595:wbd1w_4k8x0"
    }
}

enum class Flavors(val flavorName: String) {
    QA("qaFlavor"), PROD("prodFlavor")
}
