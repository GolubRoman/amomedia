package io.holub.amomedia.common.io.db

import android.content.Context
import android.content.SharedPreferences
import com.annimon.stream.Stream

import com.google.gson.Gson
import io.holub.amomedia.BuildConfig
import io.reactivex.Maybe
import io.reactivex.Single
import java.util.Arrays.asList

class Prefs(context: Context) {

    private val SHARED_PREFERENCES_KEY = "SharedPreferences"
    private val prefs: SharedPreferences
    private val prefsGson: Gson
    private val VERSION_CODE_KEY = "VERSION_CODE_KEY"

    init {
        this.prefs = context.getSharedPreferences(SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE)
        this.prefsGson = Gson()
        val versionCode = prefs.getInt(VERSION_CODE_KEY, -1)
        if (versionCode < BuildConfig.VERSION_CODE) {
            blockingClean()
            prefs.edit().putInt(VERSION_CODE_KEY, BuildConfig.VERSION_CODE).apply()
        }
    }

    fun <T> put(key: Key, value: T): Single<T> {
        return Single.fromCallable {
            blockingPut(key, value)
            value
        }
    }

    fun <T> blockingPut(key: Key, value: T?) {
        if (value != null) {
            prefs.edit().putString(key.name, prefsGson.toJson(value)).apply()
        } else {
            prefs.edit().putString(key.name, null).apply()
        }
    }

    fun <T> get(key: Key): Maybe<T> {

        val valueClass: Class<T>
        try {
            valueClass = key.valueClass as Class<T>
        } catch (e: ClassCastException) {
            throw RuntimeException(e)
        }

        return Maybe.create { emitter ->
            val value = prefs.getString(key.name, null)
            if (value == null) {
                emitter.onComplete()
            } else {
                try {
                    emitter.onSuccess(prefsGson.fromJson(value, valueClass))
                } catch (e: Exception) {
                    throw RuntimeException("Error parsing json from preferences: " + value + " to " + key.valueClass.simpleName, e)
                }

            }
        }
    }

    private fun blockingClean(vararg exceptOfKeys: Key) {
        val exceptOfKeysList = asList<Key>(*exceptOfKeys)
        Stream.of(*Key.values())
            .filter { key -> !exceptOfKeysList.contains(key) }
            .forEach { key -> blockingPut<Any>(key, null) }
    }

    enum class Key(internal var valueClass: Class<*>) {
        QUERY(String::class.java),
        INDEX(Int::class.java)
    }
}
