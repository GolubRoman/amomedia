package io.holub.amomedia.common.io.db

import android.arch.paging.DataSource
import android.arch.persistence.room.*
import io.holub.amomedia.common.io.model.QueryResult
import io.holub.amomedia.common.io.model.Request
import io.reactivex.Maybe

@Dao
interface RequestDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertRequest(request: Request): Long

    @Delete
    fun removeRequest(request: Request)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertQueryResult(queryResult: QueryResult): Long


    @Query("UPDATE Request SET `index` = :index WHERE Request.`query` = :query")
    fun updateRequestIndexForQuery(query: String, index: Int)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun upsertQueryResults(queryResults: List<QueryResult>): LongArray

    @Query("DELETE FROM Request")
    fun dropRequests()

    @Query("DELETE FROM QueryResult")
    fun dropQueryResults()

    @Delete
    fun removeQueryResult(queryResult: QueryResult)

    @Query("SELECT * FROM Request LIMIT 1")
    fun observeLastRequestMaybe(): Maybe<Request>

    @Query("SELECT * FROM QueryResult ORDER BY fetchDate, id")
    fun observeQueryResultsLiveData(): DataSource.Factory<Int, QueryResult>

    @Query("SELECT * FROM QueryResult ORDER BY fetchDate, id")
    fun observeQueryResultsMaybe(): Maybe<List<QueryResult>>

}
