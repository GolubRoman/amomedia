package io.holub.amomedia.common.view

import android.animation.Animator

interface AnimatorStartListener : Animator.AnimatorListener {

    override fun onAnimationRepeat(animation: Animator?) {}
    
    override fun onAnimationCancel(animation: Animator?) {}

    override fun onAnimationEnd(animation: Animator?) {}
}

interface AnimatorEndListener : Animator.AnimatorListener {

    override fun onAnimationRepeat(animation: Animator?) {}

    override fun onAnimationCancel(animation: Animator?) {}

    override fun onAnimationStart(animation: Animator?) {}
}