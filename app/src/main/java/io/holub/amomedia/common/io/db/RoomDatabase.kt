package io.holub.amomedia.common.io.db

import android.arch.persistence.room.Database
import io.holub.amomedia.BuildConfig
import io.holub.amomedia.common.io.model.QueryResult
import io.holub.amomedia.common.io.model.Request

@Database(
    entities = [Request::class, QueryResult::class],
    version = BuildConfig.VERSION_CODE, exportSchema = false
)
abstract class RoomDatabase : android.arch.persistence.room.RoomDatabase() {

    abstract fun getRequestDao(): RequestDao
}
