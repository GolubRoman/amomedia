package io.holub.amomedia.common.interactor

import android.arch.lifecycle.LiveData
import android.arch.paging.LivePagedListBuilder
import android.arch.paging.PagedList
import android.content.Context
import io.holub.amomedia.common.App
import io.holub.amomedia.common.io.api.GoogleCustomSearchApi
import io.holub.amomedia.common.io.db.Prefs
import io.holub.amomedia.common.io.db.RequestDao
import io.holub.amomedia.common.io.model.QueryResult
import io.holub.amomedia.common.io.model.Request
import io.holub.amomedia.common.util.buildDbConfig
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.schedulers.Schedulers
import java.util.*

class RequestInteractor(
    val context: Context,
    private val requestDao: RequestDao,
    private val googleCustomSearchApi: GoogleCustomSearchApi,
    private val prefs: Prefs
) {

    fun searchByQuery(query: String): Completable {
        return processNewQueryRequest(query)
            .andThen(observeQueryIndexSingle())
            .flatMapCompletable { index ->
                Completable.merge(
                    arrayListOf(
                        downsyncResponseForQuery(query, index),
                        downsyncResponseForQuery(query, index + 10)
                    )
                ).updateCurrentQueryIndex(query, index + 20)
            }.subscribeOn(Schedulers.io())
    }

    private fun observeQueryIndexSingle() = prefs.get<Int>(Prefs.Key.INDEX)
        .defaultIfEmpty(1)
        .toSingle()

    private fun Completable.updateCurrentQueryIndex(query: String, index: Int) =
        andThen(prefs.put(Prefs.Key.INDEX, index))
            .flatMapCompletable {
                Completable.fromRunnable { requestDao.updateRequestIndexForQuery(query, index) }
            }

    fun observeLastRequest(): Maybe<Request> = prefs.get<String>(Prefs.Key.QUERY)
        .defaultIfEmpty("")
        .flatMap { query -> prefs.get<Int>(Prefs.Key.INDEX)
            .defaultIfEmpty(1)
            .map { index -> Request("", query, index) }
        }.subscribeOn(Schedulers.io())

    /**
     * This method checks old state of cache for previous request compared to new one.
     * In case of another query it clears cache and prepares cache for new request
     *
     */
    private fun processNewQueryRequest(query: String) =
        prefs.get<String>(Prefs.Key.QUERY)
            .defaultIfEmpty("")
            .flatMapCompletable { oldQuery ->
                Completable.fromRunnable {
                    if (oldQuery.toLowerCase() != query.toLowerCase()) {
                        requestDao.dropQueryResults()

//                      Motivation of saving request data both to db and shared preferences is that
//                      according to test assignment I must save data in db, but because of saving just 1 last request
//                      I see that it`s more comfortable to maintain this data in shared preferences
                        requestDao.dropRequests()
                        requestDao.upsertRequest(Request(UUID.randomUUID().toString(), query, 1))
                        prefs.blockingPut(Prefs.Key.QUERY, query)
                        prefs.blockingPut(Prefs.Key.INDEX, 1)
                    }
                }
            }.subscribeOn(Schedulers.io())


    /**
     * This method makes query to google custom search based on input index
     * then saves data to database.
     *
     */
    private fun downsyncResponseForQuery(query: String, index: Int) = googleCustomSearchApi.getResponseList(
        App.GOOGLE_SEARCH_KEY,
        App.CUSTOM_SEARCH_ENGINE_ID,
        query,
        index
    ).map { responseDto ->
        requestDao.upsertQueryResults(responseDto.items?.map { it.mapToQueryResult() } ?: arrayListOf())
    }.toCompletable()
        .subscribeOn(Schedulers.newThread())

    fun observeQueryResults(pageSize: Int): LiveData<PagedList<QueryResult>> =
        LivePagedListBuilder(requestDao.observeQueryResultsLiveData(), buildDbConfig(pageSize)).build()
}
