package io.holub.amomedia.common.io.dto.response

data class ResponseDto(
    val context: Context?,
    val items: List<Item>?,
    val kind: String?,
    val queries: Queries?,
    val searchInformation: SearchInformation?,
    val url: Url?
)