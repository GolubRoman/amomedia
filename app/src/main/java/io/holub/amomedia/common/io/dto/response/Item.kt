package io.holub.amomedia.common.io.dto.response

import io.holub.amomedia.common.io.model.QueryResult
import java.util.*

data class Item(
    val cacheId: String?,
    val displayLink: String?,
    val formattedUrl: String?,
    val htmlFormattedUrl: String?,
    val htmlSnippet: String?,
    val htmlTitle: String?,
    val kind: String?,
    val link: String?,
    val pagemap: Pagemap?,
    val snippet: String?,
    val title: String?
) {

    fun mapToQueryResult() = QueryResult(
        cacheId?: UUID.randomUUID().toString(),
        title?: "None",
        link?: "https://www.google.com.ua/",
        snippet?: "None",
        pagemap?.cse_image?.get(0)?.src?: "",
        System.currentTimeMillis()
    )
}