package io.holub.amomedia.search

import android.animation.ObjectAnimator
import android.annotation.SuppressLint
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Intent
import android.databinding.DataBindingUtil
import android.net.Uri
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.view.KeyEvent.KEYCODE_ENTER
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.EditText
import io.holub.amomedia.R
import io.holub.amomedia.common.error.ErrorHandler
import io.holub.amomedia.common.io.model.Request
import io.holub.amomedia.common.util.hideEmptyListView
import io.holub.amomedia.common.util.hideOnSearchProgressView
import io.holub.amomedia.common.util.showEmptyListView
import io.holub.amomedia.common.util.showOnSearchProgressView
import io.holub.amomedia.common.view.AfterTextChangedListener
import io.holub.amomedia.common.view.EndlessScrollListener
import io.holub.amomedia.databinding.ActivitySearchBinding
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable

class SearchActivity : AppCompatActivity() {

    private var binding: ActivitySearchBinding? = null
    private var viewModel: SearchViewModel? = null
    private val adapter: QueryResultsAdapter = QueryResultsAdapter()
    private var queryResultClickDisposable: Disposable? = null
    private var currentRequestDisposable: Disposable? = null
    private var currentAnimator: ObjectAnimator? = null
    private var recyclerViewScrollListener: EndlessScrollListener? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_search)
        viewModel = ViewModelProviders.of(this).get(SearchViewModel::class.java)

        initSearchView()
        initRecyclerView()
    }

    override fun onDestroy() {
        super.onDestroy()
        currentRequestDisposable?.dispose()
        queryResultClickDisposable?.dispose()
    }

    @SuppressLint("CheckResult")
    private fun initSearchView() {
        binding?.ivSearch?.setOnClickListener { performSearch() }
        binding?.ivClear?.setOnClickListener { clickOnClear() }
        binding?.fabStopSearch?.setOnClickListener { stopSearch() }
        binding?.etSearch?.setOnEditorActionListener { _, actionId, event ->
            if (event != null && event.keyCode == KEYCODE_ENTER || actionId == EditorInfo.IME_ACTION_DONE) { performSearch() }
            false
        }
        viewModel?.observeLastRequest()
            ?.subscribe({ fillSearchEditText(it) }, { ErrorHandler.logError(it) })
        processClearButtonVisibility()
    }

    private fun fillSearchEditText(request: Request) {
        binding?.etSearch?.setText(request.query)
        binding?.etSearch?.setSelection(request.query.length)
    }

    private fun initRecyclerView() {
        binding?.rvResponse?.layoutManager = LinearLayoutManager(this)
        binding?.rvResponse?.adapter = adapter
        queryResultClickDisposable = adapter.observeQueryResultClickEmitter()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({ openBrowserForLink(it.link) }, { ErrorHandler.logError(it) })

        viewModel?.observeQueryResults()?.observe(this, Observer {
            it?.let {
                if (it.isEmpty()) {
                    showEmptyListView(binding?.rvResponse, binding?.llEmptyList)
                } else {
                    hideEmptyListView(binding?.rvResponse, binding?.llEmptyList)
                    adapter.submitList(it)
                }
            }
        })

        recyclerViewScrollListener = object : EndlessScrollListener() {
            override fun onLoadMore() = performSearch()
        }
        recyclerViewScrollListener?.apply { binding?.rvResponse?.addOnScrollListener(this) }

    }

//    Progress bar section
    private fun showProgress() {
        currentAnimator = showOnSearchProgressView(listOf(binding?.pbSearchProgress, binding?.fabStopSearch), currentAnimator)
    }

    private fun hideProgress() {
        currentAnimator = hideOnSearchProgressView(listOf(binding?.pbSearchProgress, binding?.fabStopSearch), currentAnimator)
    }

//    Browser section
    private fun openBrowserForLink(link: String) {
        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(link))
        startActivity(browserIntent)
    }

//    Click performers section
    @SuppressLint("CheckResult")
    private fun performSearch() {
        if (currentRequestDisposable?.isDisposed == false) { currentRequestDisposable?.dispose() }

        showProgress()
        currentRequestDisposable = viewModel?.searchByQuery(binding?.etSearch?.text.toString())
            ?.observeOn(AndroidSchedulers.mainThread())
            ?.doOnDispose {
                hideProgress()
            }?.subscribe({
                hideProgress()
            }, {
                ErrorHandler.logError(it)
                hideProgress()
            })
    }

    private fun clickOnClear() {
        binding?.etSearch?.setText("")
    }

    private fun stopSearch() {
        currentRequestDisposable?.dispose()
        recyclerViewScrollListener?.loadingInterrupted()
    }

    private fun processClearButtonVisibility() {
        binding?.etSearch?.addTextChangedListener { text ->
            binding?.ivClear?.visibility = if (text.isNotEmpty()) {
                View.VISIBLE
            } else {
                View.INVISIBLE
            }
        }
    }
}

private inline fun EditText.addTextChangedListener(crossinline body: (String) -> Unit) {
    addTextChangedListener(object : AfterTextChangedListener {
        override fun afterTextChanged(s: Editable?) = body(s.toString())

    })
}
