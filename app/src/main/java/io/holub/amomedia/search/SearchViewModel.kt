package io.holub.amomedia.search

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.arch.paging.PagedList
import io.holub.amomedia.common.App
import io.holub.amomedia.common.di.AppComponent
import io.holub.amomedia.common.di.AppScope
import io.holub.amomedia.common.interactor.RequestInteractor
import io.holub.amomedia.common.io.model.QueryResult
import io.holub.amomedia.common.io.model.Request
import io.reactivex.Completable
import io.reactivex.Maybe
import io.reactivex.android.schedulers.AndroidSchedulers
import javax.inject.Inject

class SearchViewModel : ViewModel() {

    @Inject
    lateinit var requestInteractor: RequestInteractor

    init {
        DaggerSearchViewModel_Component.builder()
            .appComponent(App.instance.appComponent())
            .build()
            .inject(this)
    }

    fun searchByQuery(query: String): Completable {
        return requestInteractor.searchByQuery(query)
    }

    fun observeLastRequest(): Maybe<Request> = requestInteractor.observeLastRequest()
        .observeOn(AndroidSchedulers.mainThread())

    fun observeQueryResults(): LiveData<PagedList<QueryResult>> = requestInteractor.observeQueryResults(0)

    @AppScope
    @dagger.Component(dependencies = [(AppComponent::class)])
    interface Component {
        fun inject(searchViewModel: SearchViewModel)
    }
}


