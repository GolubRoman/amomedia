package io.holub.amomedia.search

import android.arch.paging.PagedListAdapter
import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import io.holub.amomedia.R
import io.holub.amomedia.common.io.model.QueryResult
import io.holub.amomedia.common.util.initImageViewViaGlide
import io.holub.amomedia.databinding.ItemQueryResultBinding
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class QueryResultsAdapter : PagedListAdapter<QueryResult, QueryResultsAdapter.QueryResultViewHolder>(
    DIFF_CALLBACK
) {

    private val queryResultClickEmitter = PublishSubject.create<QueryResult>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QueryResultViewHolder {
        val binding = DataBindingUtil.inflate<ItemQueryResultBinding>(
            LayoutInflater.from(parent.context),
            R.layout.item_query_result, parent, false
        )
        return QueryResultViewHolder(binding)
    }

    override fun onBindViewHolder(holder: QueryResultViewHolder, position: Int) {
        holder.bindTo(getItem(holder.adapterPosition))
    }

    fun observeQueryResultClickEmitter(): Observable<QueryResult> = queryResultClickEmitter

    inner class QueryResultViewHolder(val binding: ItemQueryResultBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bindTo(queryResult: QueryResult?) {
            queryResult?.let {
                binding.queryResult = queryResult
                binding.executePendingBindings()
                initImageViewViaGlide(binding.ivPhoto, queryResult.image)
                binding.root.setOnClickListener { clickOnHolder() }
            }
        }

        private fun clickOnHolder() {
            binding.queryResult?.let { queryResultClickEmitter.onNext(it) }
        }
    }

    companion object {

        val DIFF_CALLBACK: DiffUtil.ItemCallback<QueryResult> = object : DiffUtil.ItemCallback<QueryResult>() {
            override fun areItemsTheSame(
                item1: QueryResult, item2: QueryResult
            ): Boolean {
                return item1.id == item2.id
            }

            override fun areContentsTheSame(
                item1: QueryResult, item2: QueryResult
            ): Boolean {
                return item1 == item2
            }
        }
    }
}
